import vk
import random
import database
import time
from kin import *

#Russia super strana

class MyBot:
    def __init__(self, token):
        self.session = vk.Session(access_token = token) 
        self.api = vk.API(self.session)
        self.db = database.database()
        self.users = self.db.get_users()
        self.greeting = 'Привет, {0}\nЯ кино бот'
        self.messages_to_send = [] # 1 сообщение - {'user_id' : '', 'message' : '', 'attachment' : ''} 
        print(self.users)
        self.main_menu = 'Вы можете воспользоваться следующими функциями: \n 1. Найти фильм\n 2. Найти актера' 
        self.state_functions = { '0' : self.state_0, '1' : self.state_1, '2' : self.state_2, '3' : self.state_3 }
        self.messages_loop()
        
    def state_0(self, args):
        greeting = self.greeting.format(args['username'])
        self.messages_to_send.append( {'user_id' : args['user_id'], 'message' : greeting, 'attachment' : 'photo-191559066_457239026'} )
        self.users[ args['user_id'] ][1] = '1'
        self.db.update_user('1', args['user_id'])
        self.messages_to_send.append( {'user_id' : args['user_id'], 'message' : self.main_menu, 'attachment' : ''} )
        

    def state_1(self, args):
        if args['message'] in ['1', '01', 'Найти фильм']:
            self.messages_to_send.append({'user_id' : args['user_id'], 'message' : 'Напишите название фильма', 'attachment' : ''} )
            self.users[ args['user_id'] ][1] = '2'
        elif args['message'] in ['2', '02', 'Найти актера']:
            self.users[ args['user_id'] ][1] = '3'
            self.messages_to_send.append({'user_id' : args['user_id'], 'message' : 'Напишите имя актера', 'attachment' : ''} )
        else:
            self.messages_to_send.append({'user_id' : args['user_id'], 'message' : 'К сожалению, я вас не понимаю', 'attachment' : ''} )
            self.messages_to_send.append({'user_id' : args['user_id'], 'message' : self.main_menu, 'attachment' : ''} )

    def state_2(self, args):
        film = getMovieInfo(args['message'])
        if film:
            ans = 'Фильм - {0} ({1})\nГод выпуска - {2}\nЖанр: {3}\n\nСюжет: {4}\n\nАктеры: {5}\n\nДлительность - {6} мин\n\nСсылка на трейлер: {7}.'
            genres = ', '.join(film['genres'])
            actors = []
            for actor in film["actors"]:
                actors.append(actor.name)
            actors = ", ".join(actors)
            ans = ans.format(film['name'], film['enName'],film["year"], genres, film['plot'], actors, film['runtime'], film["trailer"])
            self.messages_to_send.append({'user_id' : args['user_id'], 'message' : ans, 'attachment' : ''})
            self.users[ args['user_id'] ][1] = '1'
            self.messages_to_send.append({'user_id' : args['user_id'], 'message' : self.main_menu, 'attachment' : ''} )
        else:
            self.messages_to_send.append({'user_id' : args['user_id'], 'message' : 'Невозможно найти фильм', 'attachment' : ''})

    def state_3(self, args):
        actor = getActorInfo(args["message"])
        if actor:
            roles = ''
            for role in actor['roles']:
                roles += '{0} - {1}'.format(role['movie'], role['role'])
            ans = 'Имя актёра - {0}\nДень рождение - {1}\nКогда умер - {2}\nРоли: {3}.'
            ans = ans.format(actor['name'], actor["year_b"], actor["year_d"], roles)
            self.messages_to_send.append({'user_id' : args['user_id'], 'message' : ans, 'attachment' : ''})
            self.users[ args['user_id'] ][1] = '1'
            self.messages_to_send.append({'user_id' : args['user_id'], 'message' : self.main_menu, 'attachment' : ''} )
        else:
            self.messages_to_send.append({'user_id' : args['user_id'], 'message' : "Невозможно найти актера", 'attachment' : ''})
           
    
    def messages_loop(self):
        while True:
            try:
                dialogs = self.api.messages.getDialogs(unanswered = 1, v=5.92)
            except:
                self.session = vk.Session(access_token = token) 
                self.api = vk.API(self.session)
            time.sleep(0.2)
            if dialogs['count'] != 0:
                user_id = dialogs['items'][0]['message']['user_id']
                self.check_users(user_id)
                message = dialogs['items'][0]['message']['body']
                self.edit_message(message, user_id)
                #self.send_message(ans, user_id)
            for m in self.messages_to_send:
                self.send_message(m)
                self.messages_to_send.remove(m)

    def check_users(self, user_id):
        user = self.db.get_user(user_id)
        if user == False:
            name = self.api.users.get(user_ids=user_id, v=5.92)[0]['first_name']
            self.users[str(user_id)] = [name, '0', '']
            print(self.db.add_user(user_id, name))
 
    def send_message(self, ans):
        try:
            self.api.messages.send(user_id=ans['user_id'], message = ans['message'], attachment = ans['attachment'], v=5.92, random_id=random.randint(0, 1000000))
            return True
        except Exception:
            return False

    def edit_message(self, message, user_id):
        user_state = self.users[str(user_id)][1]
        args = {}
        args['username'] = self.users[str(user_id)][0]
        args['user_state'] = user_state
        args['user_id'] = str(user_id)
        args['message'] = message
        if user_state in self.state_functions:
            self.state_functions[user_state](args)
        
        

        
bot = MyBot(token = 'fae52b4975b6b56d8b5d3abe3e00a3c945832a8af2cf89ca5e7f915f9ec3f1b615414b3e4e32f7e24c27a')
