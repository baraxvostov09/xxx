from kinopoisk.movie import Movie
from kinopoisk.person import Person


def getMovieInfo(name):
    try:
        movie = Movie.objects.search(name)
        movie = movie[0]
        movie.get_content('main_page')
        movie.get_content('trailers')
    except:
        return False
    movieInfo = {}
    movieInfo['name'] = movie.title
    movieInfo['enName'] = movie.title_en
    movieInfo['year'] = movie.year
    movieInfo['plot'] = movie.plot #сюжет
    movieInfo['runtime'] = movie.runtime #продолжительность
    movieInfo['rating'] = movie.rating
    movieInfo['genres'] = movie.genres
    movieInfo['actors'] = movie.actors
    movieInfo['trailer'] = 'https://vk.com/away.php?to=http://kinopoisk.ru/film/{0}/video/{1}'.format(movie.id, movie.trailers[0].id)
    return movieInfo

def getActorInfo(name):
    try:
        actor = Person.objects.search(name)
        actor = actor[0]
        actor.get_content('main_page')
    except:
        return False
    actorInfo = {}
    actorInfo['name'] = actor.name
    actorInfo['year_b'] = actor.year_birth
    try:
        actorInfo['year_d'] = actor.year_death
    except:
        actorInfo['year_d'] = 'Неизвестно'
    actorInfo['roles'] = []
    for role in actor.career['actor'][:10]:
        actorInfo['roles'].append( {'movie' : role.movie, 'role' : role.name} )
    return actorInfo

